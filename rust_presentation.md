---
theme: default
marp: true
paginate: true
style: |
  .columns {
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 1rem;
  }
---
<!-- https://www.youtube.com/watch?v=A3AdN7U24iU -->
<!-- https://docs.google.com/presentation/d/1Ckv7qiUWXn8cFWYX6YdzeczI7uV1cIl8J3O6wdsZfL8/edit#slide=id.g765df6a1dc_0_1324 -->
<!-- https://www.slideshare.net/rodolfoeduardofinochietti/introduction-to-rust-language-programming -->
<!-- https://slideplayer.com/slide/7526337/ -->
<!-- https://slides.com/liujunfeng/deck/fullscreen   -->
<!-- https://www.geeksforgeeks.org/introduction-to-rust-programming-language/ -->
<!-- https://en.wikipedia.org/wiki/Rust_(programming_language) -->
<!-- https://hackmd.io/@rust-ctcft/r1plN4You?print-pdf#/  principles -->

# A Quick introduction to Rust: why Rust? 

by Luc Couturier

2022
<!-- ---

- [A Quick introduction to Rust](#a-quick-introduction-to-rust) -->

---

# Best Mascot


![width:300](./files/ferri.png) ![width:200](./files/gopher.png) ![width:300 center](./files/python.png)

---
# What is Rust?

**A statically and strongly typed systems programming language**

* Statically → all types known at compilation time
* Strongly typed → typing is enforced
* System → can control hardware at a low level

also:
* Imperative with functional aspects

---
# Rust main features
Design philosophy: **“Empowering everyone to build reliable and efficient
software.”**

<div class="columns">
<div>
<ul>
<li> <b>Safety</b>
    <ul>
  <li> no null pointer & no manual memory management
  <li> no undefined behaviors
  <li> compile time checks
  <li> exhaustive pattern matching
  </ul></ul>
<ul>
<li><b>Concurrency</b>
  <ul><li>no GIL
  <li> no data races
</ul>

</div>
<div>
<li> <b>Performance</b>
<ul>
  <li> no garbage collector
  <li> no runtime
  <li> zero-cost abstraction
  <li> memory efficiency
  <li> no overhead on Foreign Function Interface (FFI)
  </ul>
</ul>
</div>
</div>

---
# Downsides of Rust

* Cyclic data structures are hard to implement
* Rather long compile times
* Strict
* Large language (many keywords and features)
* Too much hype, for some area the language is still young

---

# Where does Rust fits best?
Rust can replace C/C++ in most of the cases.
<div class="columns">
<div>
<ul>
<li> Command-line utilities
<li> Data processing
<li> Extending applications
<li> Resource-constrained environments
<li> Embedded software (still early)
</ul>
</div>
<div>
<li> Server-side applications
<li> Desktop applications (still early)
<li> Mobile (early)
<li> Web (with WebAssembly)
<li> System programming (such as OS)
</div></div>
  
---
# Modern tools: Create a new project

* Create a new project:
```shell
$ cargo new rust_demo
     Created binary (application) `rust_demo` package
```
* Running the project:
```shell
$ cargo run
   Compiling rust_demo v0.1.0 (/home/luc/Dev/rust/rust_demo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.79s
     Running `target/debug/rust_demo`
Hello, world!
```
---
# Modern tools: Create a new project
![](./files/new%20project.png)

---
# Modern tools: dependencies

* Add / rm dependencies
  ```shell
  $ cargo add serde
  ```
  ![](./files/toml.png)

---

# Modern tools:

* Release (with cross-compilation):
```shell
$ cargo build --release --target=x86_64-pc-windows-gnu
   Compiling rust_demo v0.1.0 (/home/luc/Dev/rust/rust_demo/rust_demo)
    Finished release [optimized] target(s) in 0.43s
```
* Runnning tests:
```shell
$ cargo test
   Compiling rust_demo v0.1.0 (/home/luc/Dev/rust/rust_demo/rust_demo)
    Finished test [unoptimized + debuginfo] target(s) in 0.33s
     Running unittests src/main.rs (target/debug/deps/rust_demo-a648922f00211718)

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

     Running tests/dummy_test.rs (target/debug/deps/dummy_test-2070ccfcfbb8c40a)

running 1 test
test dummy_test ... ok

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s
```

---

# Modern tools: Standardized doc

```shell
$ cargo doc --open
 Documenting serde v1.0.145
 Documenting rust_demo v0.1.0 (/home/luc/Dev/rust/rust_demo/rust_demo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.16s
    Opening /home/luc/Dev/rust/rust_demo/rust_demo/target/doc/rust_demo/index.html
```
---

# Modern tools: Standardized doc

![width:1000](./files/doc.png)

---

# Modern tools: Many crates available

![width:1000](./files/cratesio.png)

---
# Modern tools: Code analysis
* Static analysis with `rust-analyzer`
  ![](./files/error_info.png)
* Linting with `$ cargo fmt` (rustfmt)
* Common mistakes & suggestions with `clippy`
* Very good VS code support

---


# But how?

How to maintain memory safety in a concurrent program without a global GC?

---

# Scope & Ownership


![](./files/lifetimes.png)

---

# Scope & Ownership

![](./files/lifetime2.png)

---
# Scope & Ownership

![](./files/move.png)

**Ownership Rules**:

* Each value in Rust has a variable that’s called its owner.
* There can only be one owner at a time.
* When the owner goes out of scope, the value will be dropped.

---
# Rules of References

Additional rules that the compiler enforces:

* by default, all variables are not mutable 
* At any given time, you can have either one mutable reference or any number of
  immutable references.
* References must always be valid.

to comply to those rules, *lifetimes* of variables must be provided.

---

# Generics

![](./files/generics.png)

---

# Traits

![](./files/trait1.png)  

---

# Traits

![](./files/trait2.png) ![](./files/traitres.png)

---

# References
https://github.com/UgurcanAkkok/AreWeRustYet (State of Rust per field)
https://www.youtube.com/watch?v=A3AdN7U24iU (A Language for the Next 40 Years)
https://www.youtube.com/watch?v=DnT-LUQgc7s&t=246s (Considering Rust)
https://slides.com/liujunfeng/deck/fullscreen
https://www.geeksforgeeks.org/introduction-to-rust-programming-language/
https://en.wikipedia.org/wiki/Rust_(programming_language)